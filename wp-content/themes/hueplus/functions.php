<?php
	 
	function hueplus_styles() {
		wp_dequeue_style( 'style' );
		wp_dequeue_style( 'responsive' );
		wp_enqueue_style( 'style-basic-2', get_template_directory_uri() . '/style.css' );
		wp_enqueue_style( 'hueplus-main', get_stylesheet_uri() );
		wp_enqueue_style( 'style-responsive-2', get_template_directory_uri() . '/responsive.css' );
		wp_enqueue_style( 'hueplus-responsive', get_stylesheet_directory_uri(). '/responsive.css' );
		//wp_enqueue_style( 'vryn-restaurant-main', get_stylesheet_uri() );
	}
	add_action( 'wp_enqueue_scripts', 'hueplus_styles', 20 );
	
function hueplus_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Slider Shortcode', 'generic_white' ),
		'id'            => 'slider-shortcode',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );

	 
}
add_action( 'widgets_init', 'hueplus_widgets_init' );

?>